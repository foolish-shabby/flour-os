import os
import base64
import io

obfuscate = None

def wrap_a_line(line, source):
    return f"<file source=\"{source}\">{line.decode('ascii')}</file>\n"

def obfuscate_core(dir_name):
    files = os.listdir(dir_name)
    files = list(map(lambda fn: dir_name + "/" + fn, files))
    content = [[]] * (len(files))
    for i in range(len(files)):
        fn = files[i]
        if os.path.isfile(fn):
            with open(fn, "rb") as f:
                content[i] = f.readlines()
            os.unlink(fn)
        else:
            obfuscate(fn)
            files[i] = fn + ".obfuscated"
            with open(fn + ".obfuscated", "rb") as f:
                content[i] = f.readlines()
            os.unlink(fn + ".obfuscated")
            os.rmdir(fn)
    
    for i in range(len(content)):
        for j in range(len(content[i])):
            content[i][j] = base64.b64encode(content[i][j])
    
    maxlinecnt = -114514
    for i in range(len(content)):
        if len(content[i]) > maxlinecnt:
            maxlinecnt = len(content[i])
    
    result = f"<dir name=\"{dir_name}\"\n"
    for i in range(maxlinecnt):
        for j in range(len(content)):
            if i < len(content[j]):
                result += wrap_a_line(content[j][i], files[j])
            else:
                result += wrap_a_line(base64.b64encode(b'\n'), files[j])
    
    result += f"</dir>"
    
    result = base64.b64encode(result.encode('ascii')).decode('ascii')
    return result

def obfuscate(dir_name):
    s = obfuscate_core(dir_name)
    buf = io.StringIO(s)
    with open(dir_name + ".obfuscated", "w") as f:
        part = buf.read(80)
        while part:
            f.write(part)
            f.write('\n')
            part = buf.read(80)

def main(argc, argv):
    if argc < 2 or argc > 2:
        print("Usage: python obfuscator.py <dir_name>")
    else:
        obfuscate(argv[1])
        os.rmdir(argv[1])

if __name__ == '__main__':
    import sys
    argc = len(sys.argv)
    argv = sys.argv
    del sys
    main(argc, argv)
