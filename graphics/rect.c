#include "include.h"

PUBLIC void draw_rect(int x0, int y0, int x1, int y1, int c)
{
    int x, y;
    for (y = y0; y <= y1; y++) {
        for (x = x0; x <= x1; x++) {
            (binfo->vram)[y * binfo->scrnx + x] = c;
        }
    }
}