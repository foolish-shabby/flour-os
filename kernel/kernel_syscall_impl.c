#include "include.h"

PUBLIC void sys_test_kernel_syscall()
{
    PROCESS *p = p_proc_ready;
    if (p->is_sys) put_str("T");
    else put_str("E");
}

PUBLIC void sys_io_out8(int port, int data)
{
    if (p_proc_ready->is_sys) out_byte(port, data);
}

PUBLIC int sys_io_in8(int port)
{
    if (p_proc_ready->is_sys) return in_byte(port);
    return -1;
}

PUBLIC void sys_io_cli()
{
    if (p_proc_ready->is_sys) disable_int();
}

PUBLIC void sys_io_sti()
{
    if (p_proc_ready->is_sys) enable_int();
}