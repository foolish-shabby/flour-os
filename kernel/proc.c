#include "include.h"

PUBLIC void schedule()
{
    PROCESS *p;
    int greatest_ticks = 0;

    while (!greatest_ticks) {
        for (p = procctl->procs; p < procctl->procs + procctl->running; p++) {
            if (p->ticks > greatest_ticks) {
                greatest_ticks = p->ticks;
                p_proc_ready = p;
            }
        }

        if (!greatest_ticks) {
            for (p = procctl->procs; p < procctl->procs + procctl->running; p++) {
                p->ticks = p->priority;
            }
        }
    }
}

PUBLIC void process_start(task_f proc, char *name, int is_sys, int priority)
{
    int i;
    PROCESS *p_proc;

    for (i = 0; i < MAX_PROCS; i++) {
        if (procctl->procs[i].flags == 0) break;
    }

    u16 selector_ldt = SELECTOR_LDT_FIRST + 8 * i;
    p_proc = &procctl->procs[i];

    strcpy(p_proc->p_name, name);
    p_proc->pid = i;
    p_proc->is_sys = is_sys;
    p_proc->flags = 1;
    procctl->running++;
    p_proc->fd_table[0] = 0;
    p_proc->fd_table[1] = 1;
    p_proc->fd_table[2] = 2;
    int fd_idx = 3;
    while (fd_idx < MAX_FILES_OPEN_PER_PROC) {
        p_proc->fd_table[fd_idx] = -1;
        fd_idx++;
    }

    p_proc->ldt_sel = selector_ldt;
    memcpy(&p_proc->ldts[0], &gdt[SELECTOR_KERNEL_CS >> 3], sizeof(DESCRIPTOR));
    p_proc->ldts[0].attr1 = DA_C | PRIVILEGE_USER << 5;
    memcpy(&p_proc->ldts[1], &gdt[SELECTOR_KERNEL_DS >> 3], sizeof(DESCRIPTOR));
    p_proc->ldts[1].attr1 = DA_DRW | PRIVILEGE_USER << 5;
    
    p_proc->regs.cs = (0 & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | RPL_USER;
    p_proc->regs.ds = (8 & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | RPL_USER;
    p_proc->regs.es = (8 & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | RPL_USER;
    p_proc->regs.fs = (8 & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | RPL_USER;
    p_proc->regs.ss = (8 & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | RPL_USER;
    p_proc->regs.gs = (8 & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | RPL_USER;
    p_proc->regs.eip = (u32) proc;
    p_proc->regs.esp = (u32) kmalloc(0x8000);
    p_proc->regs.eflags = 0x202;
    
    p_proc->ticks = p_proc->priority = priority;

    init_descriptor(&gdt[selector_ldt >> 3],
        vir2phys(seg2phys(SELECTOR_KERNEL_DS), p_proc->ldts),
        LDT_SIZE * sizeof(DESCRIPTOR) - 1,
        DA_LDT);

    u16 *p_gdt_limit = (u16 *) (&gdt_ptr[0]);
    u32 *p_gdt_base  = (u32 *) (&gdt_ptr[2]);

    *p_gdt_limit = GDT_SIZE * sizeof(DESCRIPTOR) - 1;
    *p_gdt_base  = (u32) &gdt;

    load_gdtr(gdt_ptr);
}