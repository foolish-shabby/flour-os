#include "include.h"

PUBLIC void cstart()
{
    put_str("Welcome to FlourOS\n------- init gdt & idt -------\n");
    init_gdt();
    init_idt();
    put_str("-------descriptor tables init end-------\n");
    init_memory();
    put_str("------memory checked, mm inited------\n");
    procctl = (PROCCTL *) kmalloc(sizeof(PROCCTL));
    memset(procctl, 0, sizeof(procctl));
}