#include "include.h"

PUBLIC int sys_get_ticks()
{
    return ticks;
}

PUBLIC int sys_write(char *buf)
{
    put_str(buf);
    return strlen(buf);
}