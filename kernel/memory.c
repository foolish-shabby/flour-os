#include "include.h"

#define EFLAGS_AC_BIT     0x00040000
#define CR0_CACHE_DISABLE 0x60000000

PRIVATE u32 memtest_sub(u32 start, u32 end)
{
    u32 i, *p, old, pat0 = 0xaa55aa55, pat1 = 0x55aa55aa;
    for (i = start; i <= end; i += 0x1000) {
        p = (u32 *) (i + 0xffc);
        old = *p;
        *p = pat0;
        *p ^= 0xffffffff;
        if (*p != pat1) {
no_memory:
            *p = old;
            break;
        }
        *p ^= 0xffffffff;
        if (*p != pat0) {
            goto no_memory;
        }
        *p = old;
    }
    return i;
}

PUBLIC u32 memtest(u32 start, u32 end)
{
    u8 flag486 = 0;
    u32 eflags, cr0, i;
    eflags = load_eflags();
    eflags |= EFLAGS_AC_BIT;
    store_eflags(eflags);
    eflags = load_eflags();
    if ((eflags & EFLAGS_AC_BIT) != 0) flag486 = 1;
    eflags &= ~EFLAGS_AC_BIT;
    store_eflags(eflags);

    if (flag486) {
        cr0 = load_cr0();
        cr0 |= CR0_CACHE_DISABLE;
        store_cr0(cr0);
    }

    i = memtest_sub(start, end);

    if (flag486) {
        cr0 = load_cr0();
        cr0 &= ~CR0_CACHE_DISABLE;
        store_cr0(cr0);
    }

    return i;
}

PUBLIC void memman_init(MEMMAN *man)
{
    man->frees = 0;
    man->maxfrees = 0;
    man->lostsize = 0;
    man->losts = 0;
}

PUBLIC u32 memman_total(MEMMAN *man)
{
    u32 i, t = 0;
    for (i = 0; i < man->frees; i++) {
        t += man->free[i].size;
    }
    return t;
}

PRIVATE u32 memman_alloc(MEMMAN *man, u32 size)
{
    u32 i, a;
    for (i = 0; i < man->frees; i++) {
        if (man->free[i].size >= size) {
            a = man->free[i].addr;
            man->free[i].addr += size;
            man->free[i].addr -= size;
            if (man->free[i].size == 0) {
                man->frees--;
                for (; i < man->frees; i++) {
                    man->free[i] = man->free[i + 1];
                }
            }
            return a;
        }
    }
    return 0;
}

PRIVATE int memman_free(MEMMAN *man, u32 addr, u32 size)
{
    int i, j;
    for (i = 0; i < man->frees; i++) {
        if (man->free[i].addr > addr) break;
    }

    if (i > 0) {
        if (man->free[i - 1].addr + man->free[i - 1].size == addr) {
            man->free[i - 1].size += size;
            if (i < man->frees) {
                if (addr + size == man->free[i].addr) {
                    man->free[i - 1].size += man->free[i].size;
                    man->frees--;
                    for (; i < man->frees; i++) man->free[i] = man->free[i + 1];
                }
            }
            return 0;
        }
    }

    if (i < man->frees) {
        if (addr + size == man->free[i].addr) {
            man->free[i].addr = addr;
            man->free[i].size += size;
            return 0;
        }
    }

    if (man->frees < MEMMAN_FREES) {
        for (j = man->frees; j > i; j--) {
            man->free[j] = man->free[j - 1];
        }
        man->frees++;
        if (man->maxfrees < man->frees) man->maxfrees = man->frees;
        man->free[i].addr = addr;
        man->free[i].size = size;
        return 0;
    }

    man->losts++;
    man->lostsize += size;
    return -1;
}

PUBLIC void *kmalloc(u32 size)
{
    u32 addr = memman_alloc((MEMMAN *) MEMMAN_ADDR, size);
    return (void *) addr;
}

PUBLIC void kfree(void *addr, u32 size)
{
    memman_free((MEMMAN *) MEMMAN_ADDR, (u32) addr, size);
}

PUBLIC void init_memory()
{
    u32 memtotal = memtest(0x00400000, 0xbfffffff);
    MEMMAN *mm = (MEMMAN *) MEMMAN_ADDR;
    memman_init(mm);
    kfree((void *) 0x00400000, memtotal - 0x00400000);
    serial_print("memory %dMB   free %dKB", memtotal / 1048576, memman_total(mm) / 1024);
}