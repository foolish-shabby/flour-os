%include "sconst.inc"

_NR_test_kernel_syscall    equ 0
_NR_out8                   equ 1
_NR_in8                    equ 2
_NR_cli                    equ 3
_NR_sti                    equ 4
INT_VECTOR_KERNEL_SYS_CALL equ 0x40

global test_kernel_syscall
global io_out8, io_in8
global io_cli, io_sti

bits 32
[section .text]

test_kernel_syscall:
    mov eax, _NR_test_kernel_syscall
    int INT_VECTOR_KERNEL_SYS_CALL
    ret

io_out8:
    mov eax, _NR_out8
    mov ebx, [esp + 4]
    mov ecx, [esp + 8]
    int INT_VECTOR_KERNEL_SYS_CALL
    ret

io_in8:
    mov eax, _NR_in8
    mov ebx, [esp + 4]
    int INT_VECTOR_KERNEL_SYS_CALL
    ret

io_cli:
    mov eax, _NR_cli
    int INT_VECTOR_KERNEL_SYS_CALL
    ret

io_sti:
    mov eax, _NR_sti
    int INT_VECTOR_KERNEL_SYS_CALL
    ret
