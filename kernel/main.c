#include "include.h"

void TestA()
{
    int i = 0;
    while (1) {
        if (fifo_status(&decoded_key) > 0) {
            printf("%c", fifo_get(&decoded_key));
        }
    }
}

void TestB()
{
    int i = 0x1000;
    while (1) {
        printf("B0x%x.", i++);
        milli_delay(200);
        test_kernel_syscall();
    }
}

void TestC()
{
    int i = 0x2000;
    while (1) {
        i++;
        printf("0x%x=%d", i, i);
        milli_delay(200);
        test_kernel_syscall();
    }
}

void TestD()
{
    while (1) {
        test_kernel_syscall();
        milli_delay(200);
    }
}

u32 buf[4 * 2880];

PUBLIC int kernel_main()
{
    put_str("-------kernel_main() begins-------\n");
    put_str("-------init processes-------\n");

    process_start(TestA, "TestA", false, 1);
    /*process_start(TestB, "TestB", false, 1);
    process_start(TestC, "TestC", false, 1);
    process_start(TestD, "TSysT", true, 1);*/
    
    put_str("-------PCB filled in, ready to run-------\n");

    k_reenter = 0;
    ticks = 0;

    init_clock();
    init_keyboard();
    init_hd();
    init_fs();
        
    p_proc_ready = procctl->procs;
    restart();

    while (1);
}