#define GLOBAL_VARIABLES_HERE

#include "include.h"

PUBLIC BOOTINFO *binfo = (BOOTINFO *) 0xb02;

PUBLIC irq_handler irq_table[NR_IRQ];

PUBLIC system_call sys_call_table[NR_SYS_CALL] = {sys_get_ticks, sys_write};

PUBLIC system_call kernel_sys_call_table[NR_KERN_SYS_CALL] = {sys_test_kernel_syscall, sys_io_out8, sys_io_in8, sys_io_cli, sys_io_sti};

PUBLIC u8 *fsbuf = (u8 *) 0x700000;

PUBLIC SUPER_BLOCK *global_sb;
PUBLIC u8          block_bitmap_bits[0x410 * 512];
PUBLIC bitmap      block_bitmap;
PUBLIC u8          inode_bitmap_bits[0x8 * 512];
PUBLIC bitmap      inode_bitmap;
PUBLIC list        open_inodes;