#ifndef _FLOUROS_PROTO_H_
#define _FLOUROS_PROTO_H_

// driver/serial.c
PUBLIC u8 read_serial(int port);
PUBLIC void write_serial(int port, u8 a);

// driver/keyboard.c
PUBLIC void keyboard_handler(int irq);
PUBLIC void init_keyboard();
PUBLIC void task_key();

// driver/hd.c
PUBLIC void ide_initialize(u32 BAR0, u32 BAR1, u32 BAR2, u32 BAR3, u32 BAR4);
PUBLIC void ide_read_sectors(u8 drive, u8 numsects, u32 lba, u16 es, u32 edi);
PUBLIC void ide_write_sectors(u8 drive, u8 numsects, u32 lba, u16 es, u32 edi);

#define init_hd() (ide_initialize(0x1f0, 0x3f6, 0x170, 0x376, 0x000))
#define hd_read(drive, lba, buffer, numsects) (ide_read_sectors(drive, numsects, lba, 0, (u32) buffer))
#define hd_write(drive, lba, buffer, numsects) (ide_write_sectors(drive, numsects, lba, 0, (u32) buffer))

// fs/fs.c
PUBLIC void init_fs();

// graphics/rect.c
PUBLIC void draw_rect(int x0, int y0, int x1, int y1, int c);

// graphics/text.c
PUBLIC void put_char(char c);
PUBLIC void put_str(char *s);
PUBLIC int getX();
PUBLIC void setX(int);
PUBLIC int getY();
PUBLIC void setY(int);

// kernel/i8259.c
PUBLIC void init_8259A();
PUBLIC void spurious_irq(int irq);
PUBLIC void put_irq_handler(int irq, irq_handler handler);

// kernel/kernel.asm
PUBLIC void restart();

// kernel/clock.c
PUBLIC void clock_handler(int irq);
PUBLIC void milli_delay(int ms);
PUBLIC void init_clock();

// kernel/fifo.c
PUBLIC void fifo_init(FIFO *fifo, int size, u32 *buf);
PUBLIC int fifo_put(FIFO *fifo, u32 data);
PUBLIC int fifo_get(FIFO *fifo);
PUBLIC int fifo_status(FIFO *fifo);

// kernel/main.c
void TestA();
void TestB();
void TestC();
void TestD();

// kernel/memory.c
PUBLIC void memman_init(MEMMAN *memman);
PUBLIC void kfree(void *addr, u32 size);
PUBLIC void *kmalloc(u32 size);
PUBLIC u32 memman_total(MEMMAN *memman);
PUBLIC u32 memtest(u32 start, u32 end);
PUBLIC void init_memory();

// kernel/gdt.c
PUBLIC u32 seg2phys(u16 seg);
PUBLIC void init_descriptor(DESCRIPTOR *p_desc, u32 base, u32 limit, u16 attribute);
PUBLIC void init_gdt_descriptors();
PUBLIC void init_gdt();

// kernel/proc.c
PUBLIC void schedule();
PUBLIC void process_start(task_f proc, char *name, int is_sys, int priority);

// kernel/intr.c
PUBLIC void init_interruptions();
PUBLIC void init_idt();

// lib/bitmap.c
PUBLIC void bitmap_init(bitmap *btmp);
PUBLIC bool bitmap_scan_test(bitmap *btmp, u32 bit_idx);
PUBLIC int bitmap_scan(bitmap *btmp, u32 cnt);
PUBLIC void bitmap_set(bitmap *btmp, u32 bit_idx, char value);

// lib/list.c
PUBLIC void list_init(list *plist);
PUBLIC void list_insert_before(list_elem *before, list_elem *elem);
PUBLIC void list_push(list *plist, list_elem *elem);
PUBLIC void list_append(list *plist, list_elem *elem);
PUBLIC void list_remove(list_elem *elem);
PUBLIC list_elem *list_pop(list *plist);
PUBLIC bool list_empty(list *plist);
PUBLIC u32 list_len(list *plist);
PUBLIC list_elem *list_traversal(list *plist, function func, int arg);
PUBLIC bool list_find(list *plist, list_elem *obj_elem);

// lib/debug.c
PUBLIC void panic_spin(char *filename, int line, const char *func, const char *condition);

// lib/kliba.asm
PUBLIC void out_byte(u16 port, u8 data);
PUBLIC u8 in_byte(u16 port);
PUBLIC void out_word(u16 port, u16 data);
PUBLIC u16 in_word(u16 port);
PUBLIC void out_dword(u16 port, u32 data);
PUBLIC u32 in_dword(u16 port);
PUBLIC void disable_irq(int irq);
PUBLIC void enable_irq(int irq);
PUBLIC void disable_int();
PUBLIC void enable_int();
PUBLIC u32 load_eflags();
PUBLIC void store_eflags(u32 eflags);
PUBLIC u32 load_cr0();
PUBLIC void store_cr0(u32 cr0);
PUBLIC void load_gdtr(u8 *gdtr);

// lib/klib.c
PUBLIC void put_int(int input);
PUBLIC void delay(int seconds);
PUBLIC u32 serial_print(const char *fmt, ...);
PUBLIC u32 printk(const char *fmt, ...);

// kernel system calls...

// kernel/kernel_syscall_impl.c
PUBLIC void sys_test_kernel_syscall();
PUBLIC void sys_io_out8(int port, int data);
PUBLIC int sys_io_in8(int port);
PUBLIC void sys_io_cli();
PUBLIC void sys_io_sti();

// kernel/kernel_syscall.asm
PUBLIC void kernel_sys_call();
PUBLIC void test_kernel_syscall();
PUBLIC void io_out8(int port, int data);
PUBLIC int io_in8(int port);
PUBLIC void io_cli();
PUBLIC void io_sti();

// system calls...

// kernel/proc.c
PUBLIC int sys_get_ticks();
PUBLIC int sys_write(char *buf);

// kernel/syscall.asm
PUBLIC void sys_call();
PUBLIC int get_ticks();
PUBLIC int write(char *buf);

#endif