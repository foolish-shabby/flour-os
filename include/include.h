#ifndef _FLOUROS_INCLUDE_H_
#define _FLOUROS_INCLUDE_H_

#include "type.h"
#include "const.h"
#include "protect.h"
#include "binfo.h"
#include "exceptions.h"
#include "interruptions.h"
#include "assert.h"
#include "proc.h"
#include "memory.h"
#include "bitmap.h"
#include "list.h"
#include "fifo.h"
#include "hd.h"
#include "fs.h"
#include "super_block.h"
#include "inode.h"
#include "dir.h"
#include "proto.h"
#include "keyboard.h"
#include "stdarg.h"
#include "stdio.h"
#include "string.h"
#include "global.h"

#endif