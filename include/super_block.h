#ifndef _FLOUROS_SUPER_BLOCK_H_
#define _FLOUROS_SUPER_BLOCK_H_

typedef struct super_block {
    u32 magic;

    u32 sec_cnt;
    u32 inode_cnt;
    u32 part_lba_base;

    u32 block_bitmap_lba;
    u32 block_bitmap_sects;

    u32 inode_bitmap_lba;
    u32 inode_bitmap_sects;

    u32 inode_table_lba;
    u32 inode_table_sects;

    u32 data_start_lba;
    u32 root_inode_no;
    u32 dir_entry_size;

    u8 pad[460];
} __attribute__((packed)) SUPER_BLOCK;

#endif