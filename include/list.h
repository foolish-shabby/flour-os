#ifndef _STATUE_LIST_H_
#define _STATUE_LIST_H_

#define offset(struct_type, member) ((int) (&((struct_type *) 0)->member))
#define elem2entry(struct_type, struct_member_name, elem_ptr) ((struct_type *) (((int) elem_ptr) - offset(struct_type, struct_member_name)))

struct list_elem {
    struct list_elem *prev;
    struct list_elem *next;
};

struct list {
    struct list_elem head;
    struct list_elem tail;
};

typedef bool function(struct list_elem *list, int arg);

typedef struct list list;
typedef struct list_elem list_elem;

#endif