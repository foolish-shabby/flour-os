#ifndef _FLOUROS_CONST_H_
#define _FLOUROS_CONST_H_

#define PUBLIC
#define PRIVATE static

#define EXTERN extern

#define GDT_SIZE 128
#define IDT_SIZE 256

#define RPL_KRNL 0
#define RPL_TASK 1
#define RPL_USER 3

#define NR_SYS_CALL      2
#define NR_KERN_SYS_CALL 5

#define NR_IRQ        16
#define CLOCK_IRQ     0
#define KEYBOARD_IRQ  1
#define CASCADE_IRQ   2
#define ETHER_IRQ     3
#define SECONDARY_IRQ 3
#define RS232_IRQ     4
#define XT_WINI_IRQ   5
#define FLOPPY_IRQ    6
#define PRINTER_IRQ   7
#define AT_WINI_IRQ   14

#define TIMER0         0x40
#define TIMER_MODE     0x43
#define RATE_GENERATOR 0x34
#define TIMER_FREQ     1193182L
#define HZ             100

#define KB_DATA  0x60
#define KB_CMD   0x64
#define LED_CODE 0xED
#define KB_ACK   0xFA

#define COM1 0x3f8

#define DIV_ROUND_UP(X, STEP) (((X) + (STEP) - 1) / (STEP))

#endif