#ifndef _FLOUROS_DIR_H_
#define _FLOUROS_DIR_H_

typedef struct dir {
    INODE *inode;
    u32 dir_pos;
    u8 dir_buf[512];
} DIR;

typedef struct dir_entry {
    char filename[MAX_FILE_NAME_LEN];
    u32 i_no;
    enum file_types f_type;
} DIR_ENTRY;

#endif