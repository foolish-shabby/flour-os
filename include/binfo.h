#ifndef _FLOUROS_BINFO_H_
#define _FLOUROS_BINFO_H_

typedef struct __attribute__((packed)) s_bootinfo {
    u16 magic, vmode, scrnx, scrny;
    u32 *vram;
} BOOTINFO;

#endif