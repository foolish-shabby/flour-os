#ifndef _STATUE_BITMAP_H_
#define _STATUE_BITMAP_H_

#define BITMAP_MASK 1

struct bitmap {
    u32 btmp_bytes_len;
    u8 *bits;
};

typedef struct bitmap bitmap;

#endif