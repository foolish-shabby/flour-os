#ifndef _FLOUROS_TYPE_H_
#define _FLOUROS_TYPE_H_

typedef unsigned int   u32;
typedef unsigned short u16;
typedef unsigned char  u8;

typedef void (*int_handler)();
typedef void (*irq_handler)(int irq);
typedef void (*task_f)();

typedef void *system_call;

typedef unsigned char  bool;
#define false 0
#define true 1

#endif