#ifndef _FLOUROS_GLOBAL_H_
#define _FLOUROS_GLOBAL_H_

#ifdef GLOBAL_VARIABLES_HERE
#undef EXTERN
#define EXTERN
#endif

EXTERN u8          gdt_ptr[6];
EXTERN DESCRIPTOR  gdt[GDT_SIZE];
EXTERN u8          idt_ptr[6];
EXTERN GATE        idt[IDT_SIZE];

EXTERN TSS         tss;
EXTERN PROCESS    *p_proc_ready;

EXTERN u32         k_reenter;
EXTERN int         ticks;

EXTERN PROCCTL    *procctl;
EXTERN FIFO        keyfifo;
EXTERN u32         keybuf[32];
EXTERN FIFO        decoded_key;
EXTERN u32         dkey_buf[32];

extern BOOTINFO   *binfo;
extern char        font[4096];
extern irq_handler irq_table[];
extern system_call sys_call_table[];
extern system_call kernel_sys_call_table[];
extern u32         keymap[];
extern u8         *fsbuf;
extern SUPER_BLOCK *global_sb;
extern u8          block_bitmap_bits[];
extern bitmap      block_bitmap;
extern u8          inode_bitmap_bits[];
extern bitmap      inode_bitmap;
extern list        open_inodes;

#endif