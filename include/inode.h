#ifndef _FLOUROS_INODE_H_
#define _FLOUROS_INODE_H_

typedef struct inode {
    u32 i_no;
    u32 i_size;

    u32 i_open_cnts;
    bool write_deny;

    u32 i_sectors[13];
    list_elem inode_tag;
} INODE;

#endif