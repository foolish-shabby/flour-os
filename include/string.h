#ifndef _FLOUROS_STRING_H_
#define _FLOUROS_STRING_H_

#include "type.h"
#include "const.h"

PUBLIC void *memcpy(void *dest, const void *src, int size);
PUBLIC void  memset(void *dest, u8 value, u32 size);
PUBLIC char *strcpy(char *dest, const char *src);
PUBLIC u32   strlen(const char *str);

#define NULL ((void *)0)

#endif