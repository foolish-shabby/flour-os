#ifndef _FLOUROS_PROC_H_
#define _FLOUROS_PROC_H_

#define MAX_FILES_OPEN_PER_PROC 16

typedef struct s_stackframe {
    u32 gs;
    u32 fs;
    u32 es;
    u32 ds;
    u32 edi;
    u32 esi;
    u32 ebp;
    u32 kernel_esp;
    u32 ebx;
    u32 edx;
    u32 ecx;
    u32 eax;
    u32 retaddr;
    u32 eip;
    u32 cs;
    u32 eflags;
    u32 esp;
    u32 ss;
} STACK_FRAME;

typedef struct s_proc {
    STACK_FRAME regs;

    u16 ldt_sel;
    DESCRIPTOR ldts[LDT_SIZE];

    int ticks;
    int priority;

    u32 pid;
    char p_name[16];
    int is_sys;
    int flags;

    int fd_table[MAX_FILES_OPEN_PER_PROC];
} PROCESS;

typedef struct s_task {
    task_f initial_eip;
    int stacksize;
    char name[32];
    int is_sys;
} TASK;

#define NR_TASKS 4

#define STACK_SIZE_TOTAL (STACK_SIZE_TESTA + \
                        STACK_SIZE_TESTB + \
                        STACK_SIZE_TESTC + \
                        STACK_SIZE_TESTD)

#define STACK_SIZE_TESTA 0x8000
#define STACK_SIZE_TESTB 0x8000
#define STACK_SIZE_TESTC 0x8000
#define STACK_SIZE_TESTD 0x8000

#define MAX_PROCS 1024

typedef struct s_procctl {
    int running;
    PROCESS procs[MAX_PROCS];
} PROCCTL;

#endif