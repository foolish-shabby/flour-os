#ifndef _FLOUROS_FS_H_
#define _FLOUROS_FS_H_

#define MAX_FILES_PER_PART 4096

#define BITS_PER_SECTOR 4096
#define SECTOR_SIZE 512
#define BLOCK_SIZE SECTOR_SIZE

#define MAX_FILE_NAME_LEN 256

enum file_types {
    FT_UNKNOWN,
    FT_REGULAR,
    FT_DIRECTORY
};

#endif