#ifndef _FLOUROS_ASSERT_H_
#define _FLOUROS_ASSERT_H_

#define assert(condition) \
if (condition); \
else panic(#condition);

#define panic(...) panic_spin(__FILE__, __LINE__, __func__, __VA_ARGS__)

#endif