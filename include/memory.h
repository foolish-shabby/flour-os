#ifndef _FLOUROS_MEMORY_H_
#define _FLOUROS_MEMORY_H_

#define MEMMAN_FREES 4090
#define MEMMAN_ADDR  0x00600000

typedef struct s_freeinfo {
    u32 addr, size;
} FREEINFO;

typedef struct s_memoman {
    int frees, maxfrees, lostsize, losts;
    FREEINFO free[MEMMAN_FREES];
} MEMMAN;

#endif