#ifndef _FLOUROS_STDIO_H_
#define _FLOUROS_STDIO_H_

#include "stdarg.h"
#include "type.h"
#include "const.h"

PUBLIC u32 printf(const char *fmt, ...);
PUBLIC u32 vsprintf(char *str, const char *format, va_list ap);
PUBLIC u32 printf(const char *format, ...);

#endif