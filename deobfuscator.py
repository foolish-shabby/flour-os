import base64
import io
import xml.etree.ElementTree as ET
import os.path as op
import os

def dewrap_a_line(line):
    # split file and original line from a line. e.g. <file src="printf.c">#include "include.h"</file> => printf.c, #include "include.h".
    # step 1: find <file and </file>.
    start_idx = line.find('<file')
    end_idx = line.find('</file>')
    new_line = line[start_idx:end_idx]
    # now the </file> has deleted.
    # step 2: split at '>'.
    fn, src = new_line.split(">")
    # now the source part has been detached (in src variable).
    # step 3: find '"' in the fn part. use find and rfind to find the first and last pos.
    fn_start = fn.find('"')
    fn_end = fn.rfind('"')
    new_fn = fn[fn_start + 1:fn_end]
    # now both of them are detached. congrats :)
    return [new_fn, src]

def deobfuscate_core(obfuscated_name):
    total_file_content = ""
    with open(obfuscated_name, "r") as f:
        line = f.readline()
        while line:
            total_file_content += line.rstrip()
            line = f.readline()
    
    xml_format = base64.b64decode(total_file_content).rstrip().decode('ascii')
    file_contents = xml_format.splitlines()[1:-1]

    writed = {}
    mode = "wb"

    for line in file_contents:
        fn, src_base64 = dewrap_a_line(line)
        src = base64.b64decode(src_base64)
        if fn in writed:
            mode = "ab"
        
        if not op.exists(op.split(fn)[0]):
            os.mkdir(op.split(fn)[0])
        with open(fn, mode) as f:
            f.write(src)
            writed[fn] = True
    
    files = os.listdir(op.splitext(obfuscated_name)[0])
    files = list(map(lambda fn: op.splitext(obfuscated_name)[0] + "/" + fn, files))
    for file in files:
        text = ""
        with open(file, "rb") as f:
            text = f.read().strip()
        with open(file, "wb") as f:
            f.write(text)
        
    for file in files:
        if op.splitext(file)[1][1:] == "obfuscated":
            deobfuscate(op.splitext(file)[0])

def deobfuscate(fn):
    fn += ".obfuscated"
    deobfuscate_core(fn)
    os.unlink(fn)

def main():
    import sys
    if len(sys.argv) != 2:
        print("Usage: python deobfuscator.py <obfuscated-name> (with out .obfuscated)")
    else:
        deobfuscate(sys.argv[1])

if __name__ == "__main__":
    main()