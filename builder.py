import os
import os.path as op

verbose = False

def execute(build_command):
    if verbose:
        print(build_command)
    if os.system(build_command) != 0:
        print("Build fail, process stopped.")
        os._exit(0)

def build_to_boot(source, output):
    print("Compiling: " + source)
    build_command = "nasm -I boot/include/ -o "
    build_command += output
    build_command += " "
    build_command += source
    execute(build_command)

def build_boot():
    files = os.listdir("./boot")
    def is_asm(fn): return fn.endswith(".asm")
    def to_output(fn): return "out/" + fn.replace(".asm", ".bin")
    def add_path(fn): return "boot/" + fn
    sources = list(filter(is_asm, files))
    outputs = list(map(to_output, sources))
    sources = list(map(add_path, sources))
    assert len(outputs) == len(sources)
    for i in range(len(outputs)):
        build_to_boot(sources[i], outputs[i])

def build_kernel_file(source, output, is_c):
    print("Compiling: " + source)
    build_command = ""
    if is_c:
        build_command = "gcc -c -O0 -I include/ -fno-builtin -fno-stack-protector "
        if os.name == "posix":
            build_command += "-m32 "
        else:
            build_command = "i686-elf-" + build_command
    else:
        build_command = "nasm -f elf -I include/  "
    
    build_command += "-o "
    build_command += output
    build_command += " "
    build_command += source
    execute(build_command)

def build_kernel_path(path):
    files = os.listdir(path)
    def is_src(fn): return fn.endswith(".asm") or fn.endswith("c")
    def add_path(src): return path + "/" + src
    def to_output(fn): return "out/" + fn
    sources = list(filter(is_src, files))
    for src in sources:
        if src.endswith(".asm"):
            build_kernel_file(add_path(src), to_output(src.replace(".asm", ".o")), False)
        else:
            build_kernel_file(add_path(src), to_output(src.replace(".c", ".o")), True)

def link(objects, executable):
    obj_str = ' '.join(objects)
    link_command = "ld -Ttext 0x30400 "
    if os.name == "posix":
        link_command += "-m elf_i386 "
    else:
        link_command = "i686-elf-" + link_command
    link_command += "-o "
    link_command += executable
    link_command += " "
    link_command += obj_str
    execute(link_command)

def link_kernel():
    files = os.listdir("out")
    objects = list(filter(lambda fn: fn.endswith(".o"), files))
    objects.remove('kernel.o')
    objects.insert(0, 'kernel.o')
    objects.remove('start.o')
    objects.insert(1, 'start.o')
    objects.remove('main.o')
    objects.insert(2, 'main.o')
    def add_path(fn): return "out/" + fn
    objects = list(map(add_path, objects))
    link(objects, "out/kernel.bin")

def delete_objects():
    if os.name == "nt":
        execute("del /f /s /q out\\*.o")
    else:
        execute("rm -rf out/*.o")

def build_kernel():
    print("Building: Drivers for FlourOS")
    build_kernel_path("driver")
    print("Finished building drivers")
    print("Building: File System")
    build_kernel_path("fs")
    print("Finished building file system")
    print("Building: FlourOS Graphics")
    build_kernel_path("graphics")
    print("Finished building FlourOS Graphics")
    print("Building: kernel lib")
    build_kernel_path("lib")
    print("Finished building kernel lib")
    print("Building: main kernel")
    build_kernel_path("kernel")
    print("Finished building main kernel")
    print("Linking: FlourOS Kernel")
    link_kernel()
    print("Finished linking FlourOS Kernel")
    print("Deleting objects...")
    delete_objects()
    print("Done")

def writein():
    if os.name == "nt":
        execute("dd if=out/boot.bin of=a.img bs=512 count=1")
        execute("edimg imgin:a.img copy from:out/loader.bin to:@: copy from:out/kernel.bin to:@: imgout:a.img")
    else:
        execute("dd if=out/boot.bin of=a.img bs=512 count=1 conv=notrunc")
        execute("mkdir floppy")
        execute("sudo mount -o loop a.img ./floppy/")
        execute("sudo cp out/loader.bin ./floppy/ -fv")
        execute("sudo cp out/kernel.bin ./floppy/ -fv")
        execute("sudo umount ./floppy/")
        execute("rmdir floppy")

def clean_last_time():
    if os.name == "nt":
        execute("del /f /s /q out")
    else:
        execute("rm -rf out")
        execute("mkdir out")

def build():
    print("Start building...")
    print("Cleaning last-time result")
    clean_last_time()
    print("Done")
    print("Building: FlourOS")
    print("Building: FlourOS Bootloader")
    build_boot()
    print("Finished building FlourOS Bootloader")
    print("Building: FlourOS Kernel")
    build_kernel()
    print("Finished building FlourOS Kernel")
    print("Writing FlourOS into disk...")
    writein()
    print("Finished building FlourOS")

def show_help():
    print("Usage: python builder.py [-v] [init] [build] [run] [help] [version]")
    print("")
    print("   help: show this message")
    print("   init: init a environment with a floppy image, a hard disk image and an `out` directory")
    print("   build: build FlourOS")
    print("   run: run FlourOS")
    print("   version: show builder version")
    print("   obfuscate: make the source code of this OS obfuscated")
    print("   deobfuscate: reverse the obfuscate operation")
    print("   -v: verbose mode on (default off)")
    print("Both of these commands/options can be used in combinations")

def run():
    print("Running...")
    execute("qemu-system-i386 -drive format=raw,file=a.img,if=floppy -drive format=raw,file=80m.img,if=ide,index=0 -boot a -serial stdio")

def initenv():
    if not op.exists("80m.img"):
        print("Creating hard disk image `80m.img`...", end=' ')
        with open("80m.img", "wb") as f:
            f.write(b'\x00' * 80 * 1048576)
        print("done")
    
    if not op.exists("a.img"):
        print("Creating floppy disk image `a.img`...", end=' ')
        with open("a.img", "wb") as f:
            f.write(b'\x00' * 1474560)
        print("done")
    
    if not op.exists("out"):
        print("Creating output directory `out`...", end=' ')
        execute("mkdir out")
        print("done")
    
    print("Initialized environment at current directory")

def destroyenv():
    if op.exists("80m.img"):
        print("Deleting `80m.img`...")
        if os.name == "posix":
            execute("rm -f 80m.img")
        else:
            execute("del /f /s /q 80m.img")
        print("Done")
    
    if op.exists("a.img"):
        print("Deleting `a.img`...")
        if os.name == "posix":
            execute("rm -f a.img")
        else:
            execute("del /f /s /q a.img")
        print("Done")
    
    if op.exists("out"):
        print("Deleting `out` directory...")
        if os.name == "posix":
            execute("rm -rf out")
        else:
            execute("del /f /s /q out")
            execute("rmdir out")
        print("Done")

if __name__ == '__main__':
    import sys
    print("FlourOS Builder v0.1.0")
    argc = len(sys.argv)
    argv = sys.argv
    del sys

    if argc < 2 or "help" in argv:
        show_help()
    if "-v" in argv:
        verbose = True
    if "init" in argv:
        initenv()
    if "build" in argv:
        build()
    if "run" in argv:
        run()
    if "version" in argv:
        exit()
    if "destroy" in argv:
        destroyenv()
    
    if argv[1] == "default":
        verbose = True
        destroyenv()
        initenv()
        build()
        run()
