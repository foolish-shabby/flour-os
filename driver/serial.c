#include "include.h"

PRIVATE int serial_received(int port)
{
    return in_byte(port + 5) & 1;
}

PRIVATE int is_transmit_empty(int port)
{
    return in_byte(port + 5) & 0x20;
}

PUBLIC u8 read_serial(int port)
{
    while (serial_received(port) == 0);
    return in_byte(port);
}

PUBLIC void write_serial(int port, u8 a)
{
    while (is_transmit_empty(port) == 0);
    out_byte(port, a);
}