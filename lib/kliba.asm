%include "sconst.inc"

[section .text]

global out_byte, in_byte
global enable_irq, disable_irq
global enable_int, disable_int
global load_eflags, store_eflags
global load_cr0, store_cr0
global load_gdtr
global in_word, out_word
global in_dword
global port_read, port_write

out_byte:
    mov edx, [esp + 4]
    mov al, [esp + 8]
    out dx, al
    nop
    nop
    ret

in_byte:
    mov edx, [esp + 4]
    xor eax, eax
    in al, dx
    nop
    nop
    ret

out_word:
    mov edx, [esp + 4]
    mov ax, [esp + 8]
    out dx, ax
    nop
    nop
    ret

in_word:
    mov edx, [esp + 4]
    xor eax, eax
    in ax, dx
    nop
    nop
    ret

in_dword:
    mov edx, [esp + 4]
    xor eax, eax
    in eax, dx
    nop
    nop
    ret

disable_irq:
    mov ecx, [esp + 4]
    pushf
    cli
    mov ah, 1
    rol ah, cl
    cmp cl, 8
    jae disable_8
disable_0:
    in al, INT_M_CTLMASK
    test al, ah
    jnz dis_already
    or al, ah
    out INT_M_CTLMASK, al
    popf
    mov eax, 1
    ret
disable_8:
    in al, INT_S_CTLMASK
    test al, ah
    jnz dis_already
    or al, ah
    out INT_S_CTLMASK, al
    popf
    mov eax, 1
    ret
dis_already:
    popf
    xor eax, eax
    ret

enable_irq:
    mov ecx, [esp + 4]
    pushf
    cli
    mov ah, ~1
    rol ah, cl
    cmp cl, 8
    jae enable_8
enable_0:
    in al, INT_M_CTLMASK
    and al, ah
    out INT_M_CTLMASK, al
    popf
    ret
enable_8:
    in al, INT_S_CTLMASK
    and al, ah
    out INT_S_CTLMASK, al
    popf
    ret

disable_int:
    cli
    ret

enable_int:
    sti
    ret

load_eflags:
    pushfd
    pop eax
    ret

store_eflags:
    mov eax, [esp + 4]
    push eax
    popfd
    ret

load_cr0:
    mov eax, cr0
    ret

store_cr0:
    mov eax, [esp + 4]
    mov cr0, eax
    ret

load_gdtr:
    mov eax, [esp + 4]
    lgdt [eax]
    ret

port_read:
    mov edx, [esp + 4]
    mov edi, [esp + 8]
    mov ecx, [esp + 12]
    shr ecx, 1
    cld
    rep insw
    ret

port_write:
    mov edx, [esp + 4]
    mov edi, [esp + 8]
    mov ecx, [esp + 12]
    shr ecx, 1
    cld
    rep outsw
    ret
