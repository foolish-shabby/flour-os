#include "include.h"

PUBLIC char *tohex(char *str, int num)
{
    char *p = str;
    char ch;
    int i, flag = 0;

    *p++ = '0';
    *p++ = 'x';

    if (num == 0) *p++ = '0';
    else {
        for (i = 28; i >= 0; i -= 4) {
            ch = (num >> i) & 0xf;
            if (flag || (ch > 0)) {
                flag = 1;
                ch += '0';
                if (ch > '9') ch += 7;
                *p++ = ch;
            }
        }
    }

    *p = 0;
    return str;
}

PUBLIC void put_int(int input)
{
    char output[16];
    tohex(output, input);
    put_str(output);
}

PUBLIC void delay(int seconds)
{
    for (int k = 0; k < seconds; k++) for (int i = 0; i < 10000; i++) for (int j = 0; j < 10000; j++);
}

PRIVATE void se_print(const char *s)
{
    for (; *s; s++) write_serial(COM1, *s);
}

PUBLIC u32 serial_print(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    char buf[1024] = {0};
    u32 i = vsprintf(buf, fmt, args);
    va_end(args);
    se_print(buf);
    return i;
}

PUBLIC u32 printk(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    char buf[1024] = {0};
    u32 i = vsprintf(buf, fmt, args);
    va_end(args);
    put_str(buf);
    return i;
}