#include "include.h"

PUBLIC void panic_spin(char *filename, int line, const char *func, const char *condition)
{
    disable_int();

    setX(0); setY(0);

    draw_rect(0, 0, binfo->scrnx, binfo->scrny, 0);
    put_str("Debug Assertion Failed\n");
    put_str("In file: ");
    put_str(filename);
    put_str("\nLine ");
    put_int(line);
    put_str(", function ");
    put_str((char *) func);
    put_str("\nFailed condition: ");
    put_str((char *) condition);

    for (;;);
}