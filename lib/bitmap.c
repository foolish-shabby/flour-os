#include "include.h"

void bitmap_init(bitmap *btmp)
{
    memset(btmp->bits, 0, btmp->btmp_bytes_len);
}

bool bitmap_scan_test(bitmap *btmp, u32 bit_idx)
{
    u32 byte_idx = bit_idx / 8;
    u32 bit_odd = bit_idx % 8;
    return (btmp->bits[byte_idx] & (BITMAP_MASK << bit_odd));
}

int bitmap_scan(bitmap *btmp, u32 cnt)
{
    u32 idx_byte = 0;

    while ((0xff == btmp->bits[idx_byte]) && (idx_byte < btmp->btmp_bytes_len)) {
        idx_byte++;
    }

    assert(idx_byte < btmp->btmp_bytes_len);
    if (idx_byte == btmp->btmp_bytes_len) return -1;

    int idx_bit = 0;
    while ((u8) (BITMAP_MASK << idx_bit) & btmp->bits[idx_byte]) {
        idx_bit++;
    }

    int bit_idx_start = idx_byte * 8 + idx_bit;
    if (cnt == 1) return bit_idx_start;

    u32 bit_left = (btmp->btmp_bytes_len * 8 - bit_idx_start);
    u32 next_bit = bit_idx_start + 1;
    u32 count = 1;

    bit_idx_start = -1;
    while (bit_left-- > 0) {
        if (!(bitmap_scan_test(btmp, next_bit))) {
            count++;
        } else {
            count = 0;
        }

        if (count == cnt) {
            bit_idx_start = next_bit - cnt + 1;
            break;
        }
        next_bit++;
    }
    return bit_idx_start;
}

void bitmap_set(bitmap *btmp, u32 bit_idx, char value)
{
    assert((value == 0) || (value == 1));
    u32 byte_idx = bit_idx / 8;
    u32 bit_odd = bit_idx % 8;

    if (value) {
        btmp->bits[byte_idx] |= (BITMAP_MASK << bit_odd);
    } else {
        btmp->bits[byte_idx] &= ~(BITMAP_MASK << bit_odd);
    }
}