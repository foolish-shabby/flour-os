#include "include.h"

PRIVATE u32 total_sects = 0;

PRIVATE void hd_identify()
{
    out_byte(0x1f6, 0);
    out_byte(0x1f7, 0xec);

    port_read(0x1f0, fsbuf, SECTOR_SIZE);

    total_sects = ((int) fsbuf[61] << 16) + fsbuf[60];

    memset(fsbuf, 0, 0x100000);
}

PRIVATE void mkfs()
{
    hd_identify();
    u32 boot_sector_sects = 1;
    u32 super_block_sects = 1;
    u32 inode_bitmap_sects = DIV_ROUND_UP(MAX_FILES_PER_PART, SECTOR_SIZE);
    u32 inode_table_sects = DIV_ROUND_UP(sizeof(struct inode) * MAX_FILES_PER_PART, SECTOR_SIZE);
    u32 used_sects = boot_sector_sects + super_block_sects + inode_bitmap_sects + inode_table_sects;
    u32 free_sects = total_sects - used_sects;

    u32 block_bitmap_sects = DIV_ROUND_UP(free_sects, BITS_PER_SECTOR);
    u32 block_bitmap_bit_len = free_sects - block_bitmap_sects;
    block_bitmap_sects = DIV_ROUND_UP(block_bitmap_bit_len, BITS_PER_SECTOR);

    SUPER_BLOCK sb;
    sb.magic = 998244353;
    sb.sec_cnt = total_sects;
    sb.inode_cnt = MAX_FILES_PER_PART;
    sb.part_lba_base = 0;

    sb.block_bitmap_lba = sb.part_lba_base + 2;
    sb.block_bitmap_sects = block_bitmap_sects;

    sb.inode_bitmap_lba = sb.block_bitmap_lba + sb.block_bitmap_sects;
    sb.inode_bitmap_sects = inode_bitmap_sects;

    sb.inode_table_lba = sb.inode_bitmap_lba + sb.inode_bitmap_sects;
    sb.inode_table_sects = inode_table_sects;

    sb.data_start_lba = sb.inode_table_lba + sb.inode_table_sects;
    sb.root_inode_no = 0;
    sb.dir_entry_size = sizeof(DIR_ENTRY);

    printk("hd0 info:\n");
    printk("   magic:%d\n   part_lba_base:0x%x\n   all_sectors:0x%x\n   inode_cnt:0x%x\n   block_bitmap_lba:0x%x\n   block_bitmap_sectors:0x%x\n   inode_bitmap_lba:0x%x\n   inode_bitmap_sectors:0x%x\n   inode_table_lba:0x%x\n   inode_table_sectors:0x%x\n   data_start_lba:0x%x\n", sb.magic, sb.part_lba_base, sb.sec_cnt, sb.inode_cnt, sb.block_bitmap_lba, sb.block_bitmap_sects, sb.inode_bitmap_lba, sb.inode_bitmap_sects, sb.inode_table_lba, sb.inode_table_sects, sb.data_start_lba);

    hd_write(0, 1, &sb, 1);
    printk("   super_block_lba:0x%x\n", 1);

    u32 buf_size = (sb.block_bitmap_sects >= sb.inode_bitmap_sects ? sb.block_bitmap_sects : sb.inode_bitmap_sects);
    buf_size = (buf_size >= sb.inode_table_sects ? buf_size : sb.inode_table_sects);
    buf_size *= SECTOR_SIZE;

    u8 *buf = (u8 *) fsbuf;
    buf[0] |= 0x01;
    u32 block_bitmap_last_byte = block_bitmap_bit_len / 8;
    u8 block_bitmap_last_bit = block_bitmap_bit_len % 8;
    u32 last_size = SECTOR_SIZE - (block_bitmap_last_byte % SECTOR_SIZE);

    memset(&buf[block_bitmap_last_byte], 0xff, last_size);

    u8 bit_idx = 0;
    while (bit_idx <= block_bitmap_last_bit) {
        buf[block_bitmap_last_byte] &= ~(1 << bit_idx);
        bit_idx++;
    }
    hd_write(0, sb.block_bitmap_lba, buf, sb.block_bitmap_sects);

    memset(buf, 0, buf_size);
    buf[0] |= 0x1;

    hd_write(0, sb.inode_bitmap_lba, buf, sb.inode_bitmap_sects);

    memset(buf, 0, buf_size);
    INODE *i = (INODE *) buf;
    i->i_size = sb.dir_entry_size * 2;
    i->i_no = 0;
    i->i_sectors[0] = sb.data_start_lba;

    hd_write(0, sb.inode_table_lba, buf, sb.inode_table_sects);

    memset(buf, 0, buf_size);
    DIR_ENTRY *p_de = (DIR_ENTRY *) buf;

    memcpy(p_de->filename, ".", 1);
    p_de->i_no = 0;
    p_de->f_type = FT_DIRECTORY;
    p_de++;

    memcpy(p_de->filename, "..", 2);
    p_de->i_no = 0;
    p_de->f_type = FT_DIRECTORY;

    hd_write(0, sb.data_start_lba, buf, 1);

    printk("   root_dir_lba: 0x%x\n", sb.data_start_lba);
    printk("hd0 format done\n");

    memset(fsbuf, 0, 0x100000);
}

PRIVATE void mount_fs()
{
    SUPER_BLOCK *sb = (SUPER_BLOCK *) fsbuf;
    memset(sb, 0, SECTOR_SIZE);
    hd_read(0, 1, sb, 1);
    memcpy(global_sb, sb, sizeof(SUPER_BLOCK));

    inode_bitmap.bits = inode_bitmap_bits;
    inode_bitmap.btmp_bytes_len = sb->inode_bitmap_sects * SECTOR_SIZE;

    hd_read(0, sb->inode_bitmap_lba, inode_bitmap_bits, sb->inode_bitmap_sects);

    block_bitmap.bits = block_bitmap_bits;
    block_bitmap.btmp_bytes_len = sb->block_bitmap_sects * SECTOR_SIZE;

    hd_read(0, sb->block_bitmap_lba, block_bitmap_bits, sb->block_bitmap_sects);

    list_init(&open_inodes);
    printk("mount hd0 done!\n");
}

PUBLIC void init_fs()
{
    SUPER_BLOCK *sb = (SUPER_BLOCK *) fsbuf;

    printk("detecting file system...\n");
    memset(sb, 0, SECTOR_SIZE);

    hd_read(0, 1, sb, 1);

    if (sb->magic == 998244353) {
        printk("hd0 has file system\n");
        //mount_fs();
    } else {
        printk("no file system detected. formatting......\n");
        mkfs();
    }

    memset(fsbuf, 0, 0x100000);
}